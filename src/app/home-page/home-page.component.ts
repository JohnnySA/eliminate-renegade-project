/// <reference types="@types/googlemaps" />
import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';


@Component({
  selector: 'home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit, AfterViewInit {

  @ViewChild('gmap', {static: false}) mapContainer: any;
  map: google.maps.Map;
  navigatorOptions: any = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
  };
  mapZoom: number = 18;
  currentLongitude: any;
  currentLatitude: any;

  constructor() { }

  ngOnInit() {
    this.handlePermission();
  }

  handlePermission() {
    navigator.permissions.query({name:'geolocation'}).then((result) => {
      if (result.state == 'granted') {
        this.getLocation();
      } else if (result.state == 'prompt') {
        console.log('prompt')
      } else if (result.state == 'denied') {
        console.log('denied')
      }
    });
  }

  getLocation () {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.currentLongitude = position.coords.longitude
        this.currentLatitude = position.coords.latitude;

      }, (error) => {
        console.log(error);
      }, this.navigatorOptions);
    } else {
      console.log('0000')
    }
  }

  ngAfterViewInit() {

    setTimeout(() => {
      var myLatLng = {lat: this.currentLatitude, lng: this.currentLongitude};

      var mapProp = {
        center: new google.maps.LatLng(this.currentLatitude, this.currentLongitude),
        zoom: this.mapZoom,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      this.map = new google.maps.Map(this.mapContainer.nativeElement, mapProp);

      var marker = new google.maps.Marker({
        position: myLatLng,
        map: this.map,
        title: 'Your current location'
      });
  
    }, 2000);
  }

}
